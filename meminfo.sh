memcount=$(dmidecode -t 17 | awk 'BEGIN{RS=""} !/Type: Unknown/' | awk -v FS=": " 'BEGIN {i=0} /Size/ {i++;} END {print i}')
other=$(dmidecode -t 17 | awk 'BEGIN{RS=""; ORS="\n\n"} !/Type: Unknown/ {if (FNR==2) print}' | awk 'BEGIN{FS=": "} /Size|Form Factor|^\tSpeed|Manufacturer/ {printf "%s ", $2;}')
printf '%s x %s\n' "$memcount" "$other"

