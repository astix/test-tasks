out=$(pacman -Qc $1 2>/dev/null | sed -e "/Changelog for $1:/d" -e '/./{H;$!d;}' -e "x;/ver $2:/!d;")

if [ -z "$out" ]; then
	pacman -Qc $1
else
	printf "Changelog for $1:\n$out\n"
fi