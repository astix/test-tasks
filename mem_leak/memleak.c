#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

void leak(){
	void *p1 = malloc((rand()%10+1)*1024*1024);
	void *p2 = malloc((rand()%10+1)*1024*1024);
	void *p3 = malloc((rand()%10+1)*1024*1024);
	void *p4 = malloc((rand()%10+1)*1024*1024);
	sleep(0.02);
	free(p1);
	free(p3);
}

void noleak(){
        void *p1 = malloc((rand()%10+1)*1024*1024);
        void *p2 = malloc((rand()%10+1)*1024*1024);
        void *p3 = malloc((rand()%10+1)*1024*1024);
        void *p4 = malloc((rand()%10+1)*1024*1024);
	sleep(0.02);
        free(p1);
	free(p2);
        free(p3);
	free(p4);
}

int main(int argc, char *argv[]){
	srand(time(NULL));
	void (*ptf)();
	if (!strcmp(argv[1], "leak")){
		ptf = &leak;
	} else if (!strcmp(argv[1], "noleak")){
		ptf = &noleak;
	}
	while (1){
		(*ptf)();
	}
	return 0;
}
