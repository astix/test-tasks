echo "Starting the program '$1'"
$1 &
pid=$!

declare -a memarr
for i in {1..1000}
do
	memarr[$i]=$(ps -o vsz -p $pid | sed -n '2{p;q}' | tr -d '[:space:]')
	sleep 0.02
done
kill $pid

nsummulti=0
sumx=500500
sumy=0
nsumxsqr=333833500000

for i in {1..1000}
do
	(( nsummulti+=i*${memarr[$i]} ))
	(( sumy+=${memarr[$i]} ))
done

(( nsummulti*=1000,sumy*=sumx,nsummulti-=sumy ))
(( nsumxsqr*=1000,sumx*=sumx,nsumxsqr-=sumx ))

slope=$(bc <<< "$nsummulti/$nsumxsqr")

if [ $(bc -l <<< "$slope > 0.09") -eq "1" ]; then
	echo "The program '$1' has memory leak"
else
	echo "The program '$1' hasn't memory leak"
fi

